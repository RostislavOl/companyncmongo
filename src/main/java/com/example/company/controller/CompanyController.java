package com.example.company.controller;

import com.example.company.entities.Company;
import com.example.company.repository.CompanyRepository;
import com.example.company.service.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

@RestController
@RequestMapping(path = "/companies")
public class CompanyController {

    @Autowired
    CompanyRepository companies;

    @Autowired
    RestClient restClient;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(companies.findAll());
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public ResponseEntity<?> getUserById(@PathVariable int id) {
        Company employer = companies.findAllById(id);
        if (employer != null) {
            return ResponseEntity.ok(employer);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Company with id = " + id + " not found");
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> createEmployer(@RequestBody Company company) {
        if (company != null) {
            Company companyInRepository = companies.findAllById(company.getId());
            //обновление
            if (companyInRepository != null) {
                companies.delete(companyInRepository.getId());
                companyInRepository.setAddress(company.getAddress());
                companyInRepository.setDescription(company.getDescription());
                companyInRepository.setEmail(company.getEmail());
                companies.save(companyInRepository);
                return ResponseEntity.ok().build();
            } else {
                companies.save(company);
            }
        }
        try {
            return ResponseEntity
                    .created(new URI("/employers/" + company.getId()))
                    .body(company);
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.CREATED).body(company);
        }
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity deleteEmployer(@PathVariable int id) {
        Company company = companies.findAllById(id);
        if (company != null) {
            companies.delete(id);
        }
        try {
            restClient.deleteAllUsersByDept(id);
        }catch(Exception ex){
            System.out.println("Can't delete this users");
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}/employers")
    public ResponseEntity<?> getAllEmployers(@PathVariable int id) {
        try {
            return ResponseEntity.ok(restClient.getAllUsers(id));
        }catch(Exception ex){}
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @RequestMapping(value = "companies/{id}/employers", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity deleteEmployerByDept(@PathVariable int id) {
        System.out.println(id);
            try {
                restClient.deleteAllUsersByDept(id);
            }catch(Exception ex){
                System.out.println("Can't delete this users");
            }
        return ResponseEntity.ok().build();
    }

}
