package com.example.company.entities;

import java.io.Serializable;

public class Employer implements Serializable {

    private String jobRole;
    private int id;
    private int customerId;
    private String firstName;
    private String lastName;
    private String email;

    public Employer(){}

    public Employer(String jobRole, int employerId, int customerId, String firstName, String lastName, String email) {
        this.jobRole = jobRole;
        this.id = employerId;
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public int getEmployerId() {
        return id;
    }

    public void setEmployerId(int employerId) {
        this.id = employerId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
