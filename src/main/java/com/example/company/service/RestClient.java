package com.example.company.service;

import com.example.company.entities.Employer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Component
public class RestClient {

    RestTemplate restTemplate = new RestTemplate();
    @Value("${employer.host}")
    String employerHostName;

    public List<Employer> getAllUsers(int deptId) throws URISyntaxException {
        System.out.println("get all users with id =" + deptId);
        List<Employer> employers = restTemplate.exchange(new URI(employerHostName + "/employers/"+deptId), HttpMethod.GET,null, new ParameterizedTypeReference<List<Employer>>() {}).getBody();
        return employers;
    }

    public void deleteAllUsersByDept(int deptId) throws URISyntaxException {
        System.out.println(employerHostName + "/employers/" + deptId+"/customerId");
        restTemplate.delete(employerHostName + "/employers/" + deptId+"/customerId");
    }
}
